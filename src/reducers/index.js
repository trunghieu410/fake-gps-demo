import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'

import bookingReducer from './reducer_booking'

export default combineReducers({
  router: routerReducer,
  bookings: bookingReducer
});
