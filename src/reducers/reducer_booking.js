import { extend } from 'lodash'
import * as actions from '../global/actionTypes'

export default function (state = initialState, action = {}) {
  switch (action.type) {
    case actions.FETCH_BOOKING_ACTIONS_REQUEST:
    case actions.FETCH_BOOKING_ACTIONS_SUCCESS:
    case actions.FETCH_BOOKING_ACTIONS_FAILURE:
      return extend({}, state, action.payload)
    default:
      return state
  }
}

const initialState = {
  bookingIds: [],
  bookingDetails: []
}
