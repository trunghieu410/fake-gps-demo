import * as actions from '../global/actionTypes'
import { parseCsvToArray } from '../global/helper'


export function fetchBookingData(data) {
  return (dispatch) => {
    //Get data from API, but from CSV for now
    const bookings = parseCsvToArray(data)

    dispatch(fetchDataSuccess(bookings))
  }
}


function fetchDataSuccess(bookings) {
  return {
    type: actions.FETCH_BOOKING_ACTIONS_SUCCESS,
    payload: bookings
  };
}
