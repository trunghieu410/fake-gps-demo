import _ from 'lodash'
import geolib from 'geolib'
import * as constants from './constants'

export const getLatLng = (obj) => {
  const split = obj.coordinates.split(',')
  if (split.length === 2) {
    return { lat: parseFloat(split[0]), lng: parseFloat(split[1]) }
  }
  return {}
}

export const parseCsvToArray = (lines) => {
  try
  {
    let data = []
    let columns = lines[0]

    _.forEach(lines, (line, i) => {
      if (i === 0) { return }

      let lineObject = {}
      _.forEach(columns, (col, i) => {
        lineObject[_.camelCase(col)] = line[i]
      })

      data.push(lineObject)
    })

    return {
      bookingDetails: data,
      bookingIds: _.uniq(_.map(data, 'bookingId'))
    }
  }
  catch(err){
    return err
  }
}

export const groupBookingByStatus = (bookings) => {
  let availableBefore = []
  let availableAfter = []

  const otwPickup = _.orderBy(_.filter(bookings, function(bk) { return bk.driverStatus === constants.OTW_PICKUP }), ['seconds'], ['asc'])
  const otwDropOff = _.orderBy(_.filter(bookings, function(bk) { return bk.driverStatus === constants.OTW_DROPOFF }), ['seconds'], ['asc'])

  _.forEach(bookings, (b, i) => {
    if (b.driverStatus === constants.AVAILABLE) {
      const locationTime = parseInt(b.seconds)
      const otwPickupTime = parseInt(otwPickup[0].seconds)
      const otwDropOffTime = otwDropOff[0] ? parseInt(otwDropOff[otwDropOff.length-1].seconds) : parseInt(otwPickup[otwPickup.length-1].seconds)

      // Only show data 10 min before OTW_PICKUP and after AVAILABLE
      if (locationTime < otwPickupTime && otwPickupTime - locationTime <= constants.TEN_MINUTES_IN_SECONDS) {
        availableBefore.push(b)
      }
      if (locationTime > otwDropOffTime && locationTime - otwDropOffTime <= constants.TEN_MINUTES_IN_SECONDS) {
        availableAfter.push(b)
      }
    }
  })
  availableBefore = _.orderBy(availableBefore, ['seconds'], ['asc'])
  availableAfter = _.orderBy(availableAfter, ['seconds'], ['asc'])

  return [availableBefore, otwPickup, otwDropOff, availableAfter]
}

export const getTimeFromMiliSecond = (duration) => {
  let hour = 0
  let min = 0
  let sec = 0

  const formatTime = (time, suffix) => {
    if (time === 0) {
      return ''
    } else {
      return `${time}${suffix}`
    }
  }

  if (duration){
    if (duration >= 60){
      min = Math.floor(duration / 60)
      sec = duration % 60
    }
    else{
      sec = duration
    }

    if (min >= 60){
      hour = Math.floor(min / 60)
      min = min - hour * 60
    }
  }

  let h = formatTime(hour, 'h')
  let m = formatTime(min, 'm')
  let s = formatTime(sec, 's')

  if (h === '') {
    if (m === '') {
      s = s === '' ? '0s' : s
      return `${s}`
    } else {
      return `${m} ${s}`
    }
  } else {
    return `${h} ${m} ${s}`
  }
}

// Return the speed of the driver went from A -> B in km/h
export const getLocationSpeed = (previousStep, currentStep) => {
  const timeToCurrentStep = currentStep.seconds - previousStep.seconds
  const distance = getDistanceOfTwoLocations(getLatLng(previousStep), getLatLng(currentStep))

  let speedToNextStep = 0
  if (distance !== 0  && timeToCurrentStep !== 0) {
    speedToNextStep = Math.round((distance/1000) / (timeToCurrentStep/3600))
  }

  return speedToNextStep
}

// Return the distance from A -> B in meters
export const getDistanceOfTwoLocations = (fromLocation, toLocation) => {
  return geolib.getDistance(
    {latitude: fromLocation.lat, longitude: fromLocation.lng},
    {latitude: toLocation.lat, longitude: toLocation.lng}
  )
}
