export const GOOGLE_MAP_KEY = 'AIzaSyCttXNd7HeKn_YtYlsQd5mcRrf0AqiAKpE'

export const RED_COLOR = '#EF3945'
export const GREEN_COLOR = '#A2DC2A'

export const TELEPOST_LINE = 'TELEPOST_LINE'
export const CONSECUTIVE_LINE = 'CONSECUTIVE_LINE'
export const OTW_PICKUP = 'OTW_PICKUP'
export const OTW_DROPOFF = 'OTW_DROPOFF'
export const AVAILABLE = 'AVAILABLE'

export const ORIGIN_LOCATION = 'Customer origin location'
export const DES_LOCATION = 'Customer destination location'
export const FIRST_OTW_PICKUP_LOCATION = 'Trip Start'
export const FIRST_OTW_DROPOFF_LOCATION = 'Passenger Pickup'
export const FIRST_AVAILABLE_LOCATION = 'Trip End'

export const ANIMATION_SPPED = 1000
export const TEN_MINUTES_IN_SECONDS = 600
export const SPEED_BUTTON_VALUES = [1, 2, 2.5, 5, 10]
export const FAKE_DRIVER_SPEED_LIMIT = 60 // km/h
