import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux';
import Select from 'react-select'
import _ from 'lodash'

import CSVReader from 'react-csv-reader'

import Map from '../../components/map.js'
import { fetchBookingData } from '../../actions/action_booking'
import 'react-select/dist/react-select.css'

export class Home extends Component {
  constructor(props) {
    super(props)
    this.state = {
      selectedBookingData: [],
      selectedBookingId: null
    }
  }

  handleParseCsv(loadedData) {
    const { fetchBookingData } = this.props
    fetchBookingData(loadedData)
  }

  handleSelectChange(selectedOption) {
    const { bookings } = this.props
    const data = _.orderBy(
      _.filter(
        bookings.bookingDetails, (b) => b.bookingId === selectedOption.value
      ), ['seconds'], ['asc']
    )

    this.setState({
      selectedBookingData: data,
      selectedBookingId: selectedOption.value
    })
  }

  getSelectOptions() {
    let options = []
    _.forEach(this.props.bookings.bookingIds, (b) => {
      options.push({ value: b, label: b })
    })
    return options
  }

  render() {
    return (
      <Fragment>
        {this.props.bookings.bookingDetails.length === 0 ?
          <span style={styles.uploadBox}>
            <CSVReader onFileLoaded={this.handleParseCsv.bind(this)}/>
          </span> :
          <div style={styles.selectBox}>
            <Select
              clearable={false}
              value={this.state.selectedBookingId}
              onChange={this.handleSelectChange.bind(this)}
              options={this.getSelectOptions()}
            />
          </div>
        }

        <Map
          bookingData={this.state.selectedBookingData}
        />
      </Fragment>
    )
  }
}

const styles = {
  selectBox: {
    width:'350px',
    margin:'left',
    marginBottom:'10px',
    zIndex: '10000',
    top: '15px',
    left: '66px',
    position: 'absolute'
  },
  uploadBox: {
    position: 'absolute',
    zIndex: '1000',
    top: '24px',
    left: '66px'
  }
}

function mapStateToProps(state) {
  return {
    bookings: state.bookings
  }
}

export default connect(mapStateToProps, { fetchBookingData })(Home)




