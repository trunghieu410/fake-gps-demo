 /* global google */
import React, { Fragment, Component } from 'react'
import PropTypes from 'prop-types'
import _ from 'lodash'
import CustomMarker from './customMarker'
import fancyMapStyles from '../global/fancyMapStyles.json'
import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  Polyline
} from 'react-google-maps'
import {
  getLatLng,
  getLocationSpeed,
  groupBookingByStatus } from '../global/helper'
import * as constants from '../global/constants'
import greenLocation from '../img/green.png'
import redLocation from '../img/red.png'
import tripEnd from '../img/tripEnd.svg'
import passengerPickup from '../img/passengerPickup.svg'
import tripStart from '../img/tripStart.svg'

const GoogleMapWrapper = withScriptjs(withGoogleMap(props => {
  const { isPlaying, filteredBookingData, hightLightIndex, polylines, defaultCenter, zoom } = props

  let mapZoom =zoom
  let mapMarkers = []
  let mapPolylines = []
  let mapCenter = defaultCenter

  if (filteredBookingData.length > 0) {

    const getMapMarkerIcon = (location, isMilestoneLocation, index, dupQuantities) => {
      let markerOptions = {}
      if (isMilestoneLocation) {
        switch (location.driverStatus) {
          case constants.OTW_PICKUP:
            markerOptions.url = tripStart
            break
          case constants.OTW_DROPOFF:
            markerOptions.url = passengerPickup
            break
          case constants.AVAILABLE:
            markerOptions.url = tripEnd
            break
        }
      } else {
        markerOptions.url = dupQuantities > 3 ? redLocation : greenLocation

        if (index === hightLightIndex && isPlaying) {
          markerOptions.anchor = { x: 7, y: 7 }
          markerOptions.scaledSize= new google.maps.Size(15, 15)
        } else {
          markerOptions.anchor = { x: 5, y: 5 }
          markerOptions.scaledSize= new google.maps.Size(10, 10)
        }
      }

      return markerOptions
    }

    const milestones = _.map(props.milestones, 'location')
    mapCenter = getLatLng(filteredBookingData[hightLightIndex])
    let dupQuantities = 0
    let prevLatlng = null

    _.forEach(filteredBookingData, (b, i) => {
      const latlng = getLatLng(b)

      if (_.isEmpty(latlng)) { return }
      if (_.isEqual(latlng, prevLatlng)) {
        dupQuantities = dupQuantities + 1
      } else {
        prevLatlng = latlng
        dupQuantities = 0
      }

      if (!isPlaying) {
        mapCenter = latlng
        mapZoom = 17
      } else {
        mapZoom = 15
      }

      const isMilestoneLocation = milestones.indexOf(b) > -1
      mapMarkers.push(
        <CustomMarker
          {...props}
          index={i}
          position={latlng}
          key={`markers-${i}`}
          location={b}
          info={
            isMilestoneLocation
            ? props.milestones[milestones.indexOf(b)].type
            : i
          }
          icon={getMapMarkerIcon(b, isMilestoneLocation, i, dupQuantities)}
        />
      )
    })

    _.forEach(polylines, (line, i) => {
      mapPolylines.push(
        <Polyline
          key={`polylines-${i}`}
          path={line.coods}
          geodesic={true}
          options={line.options}
        />
      )
    })
  }

  return (
    <Fragment>
      <GoogleMap
        zoom={mapZoom}
        center={mapCenter}
        options={{
          styles: fancyMapStyles,
          mapTypeControl: false,
          fullscreenControl: false
        }}>
        { mapMarkers }
        { mapPolylines }
      </GoogleMap>
    </Fragment>
  )
}))

export class Map extends Component {
  constructor(props) {
    super(props)
    this.state = this.defaultState()
    this.initBooKingDetailOnMaps(props.bookingData)
    this.interval = null
  }

  componentWillReceiveProps(nextProps, nextState) {
    if (nextProps.bookingData !== this.props.bookingData && nextProps.bookingData.length > 0) {
      this.setState(this.defaultState(), () => {
        clearInterval(this.interval)
        this.initBooKingDetailOnMaps(nextProps.bookingData)
      })
    }
  }

  defaultState = () => {
    return {
      directions: [],
      isPause: false,
      animationSpeed: 1,
      isPlaying: false,
      isForward: true,
      milestones: [],
      polylines: [],
      filteredBookingData: [],
      hightLightIndex: 0
    }
  }

  getAnimationSpeed = () => {
    return parseInt(constants.ANIMATION_SPPED) / this.state.animationSpeed
  }

  increaseShowingLocations = () => {
    const { hightLightIndex, isForward, filteredBookingData } = this.state
    let stopHighlightLocation = isForward ? filteredBookingData.length - 1 : 0
    let nextHighlightLocation = isForward ? hightLightIndex + 1 : hightLightIndex - 1

    if (hightLightIndex === stopHighlightLocation) {
      this.handleStopAnimation()
    } else {
      this.setState({ hightLightIndex: nextHighlightLocation })
    }
  }

  setPlayInterval = (time) => {
    clearInterval(this.interval)
    this.interval = setInterval( this.increaseShowingLocations , time)
  }

  onPlayPauseClick = () => {
    const { isPlaying } = this.state

    if (!isPlaying) {
      this.setState({ isPlaying: true, isForward: true }, () => {
        this.setPlayInterval(this.getAnimationSpeed())
      })
    } else {
      this.handlePauseAnimation()
    }
  }

  onMarkerClick = (index) => {
    this.setState({ hightLightIndex: index })
  }

  handleStopAnimation = () => {
    clearInterval(this.interval)
    this.setState({
      animationSpeed: 1,
      isPause: false,
      isPlaying: false,
      isForward: true,
      hightLightIndex: 0
    })
  }

  handlePauseAnimation = () => {
    if (this.state.isPause) {
      this.setState({ isPause: false }, () => {
        this.setPlayInterval(this.getAnimationSpeed())
      })
    } else {
      this.setState({ isPause: true }, () => {
        clearInterval(this.interval)
      })
    }
  }

  handleOnChangeSpeed = (newSpeed) => {
    const { animationSpeed } = this.state
    if (animationSpeed !== newSpeed) {
      const newSpeedInSeconds = constants.ANIMATION_SPPED / newSpeed

      this.setState({ animationSpeed: newSpeed, isPause: false }, () => {
        this.setPlayInterval(newSpeedInSeconds)
      })
    }
  }

  handleStartOver = () => {
    if (this.state.isPlaying) {
      this.setState({
        isPause: false,
        animationSpeed: 1,
        isPlaying: true,
        isForward: true,
        hightLightIndex: 0
      }, () => {
        this.setPlayInterval(this.getAnimationSpeed())
      })
    }
  }

  handleOneStepMove = (d) => {
    const { isPause, isPlaying, hightLightIndex, filteredBookingData } = this.state

    if (isPlaying && isPause) {
      if (d === 1) {
        if (hightLightIndex === filteredBookingData.length-1) {
          this.handleStopAnimation()
        } else {
          this.setState({ isForward: true, hightLightIndex: hightLightIndex + 1 })
        }
      } else {
        if (hightLightIndex === 0) {
          this.handleStopAnimation()
        } else {
          this.setState({ isForward: false, hightLightIndex: hightLightIndex - 1 })
        }
      }
    }
  }

  generatePolylines = (bookingData, milestones) => {
    const passPickupLocation = _.filter(milestones, function(m) { return m.type === constants.FIRST_OTW_DROPOFF_LOCATION })
    const passPickupTime = passPickupLocation[0] ? passPickupLocation[0].location.seconds : bookingData[0].seconds
    let polylines = []

    _.forEach(bookingData, (currentStep, index) => {
      if (index === 0) {
        return
      }

      const previousStep = bookingData[index - 1]

      const previousLocation = getLatLng(previousStep)
      const currentStepLocation = getLatLng(currentStep)
      const speedToCurrentStep = getLocationSpeed(previousStep, currentStep)

      const type = speedToCurrentStep > constants.FAKE_DRIVER_SPEED_LIMIT ? constants.TELEPOST_LINE : constants.CONSECUTIVE_LINE
      const strokeColor = type === constants.TELEPOST_LINE ? constants.RED_COLOR : constants.GREEN_COLOR

      let polylineOptions = {
        strokeColor: strokeColor,
        strokeOpacity: 1.0,
        strokeWeight: 3,
      }

      if (currentStep.seconds <= passPickupTime) {
        polylineOptions = {
          strokeColor: strokeColor,
          strokeOpacity: 0,
          strokeWeight: 0,
          icons: [{
            icon: {
              path: google.maps.SymbolPath.CIRCLE,
              fillOpacity: 1,
              scale: 3
            },
            offset: '0',
            repeat: '10px'
          }]
        }
      }

      polylines.push({
        type: type,
        coods: [previousLocation, currentStepLocation],
        options: polylineOptions
      })
    })

    return polylines
  }

  initBooKingDetailOnMaps = (bookingData) => {
    if (bookingData.length > 0) {
      let milestones = []

      const groupedBookingsByStatus = groupBookingByStatus(bookingData)
      const filteredBookingData = _.flatten(groupedBookingsByStatus)

      // // Customer origin location (need to query db later)
      // milestones.push({
      //   type: 'ORIGIN_LOCATION',
      //   location: groupedBookingsByStatus[0][0]
      // })

      _.forEach(groupedBookingsByStatus, (mode, i) => {
        if (i !== 0 && mode[0] !== undefined) {
          let type
          if (mode[0].driverStatus === constants.OTW_PICKUP) {
            type = constants.FIRST_OTW_PICKUP_LOCATION
          } else if (mode[0].driverStatus === constants.OTW_DROPOFF) {
            type = constants.FIRST_OTW_DROPOFF_LOCATION
          } else if (mode[0].driverStatus === constants.AVAILABLE) {
            type = constants.FIRST_AVAILABLE_LOCATION
          }

          milestones.push({
            type: type,
            location: mode[0]
          })
        }
      })

      const polylines = this.generatePolylines(filteredBookingData, milestones)

      this.setState({
        polylines: polylines,
        milestones: milestones,
        filteredBookingData: filteredBookingData
      })
    }
  }

  isDisableButton = (buttonSpeed) => {
    if (this.state.animationSpeed === buttonSpeed) {
      return true
    }
    return false
  }

  getPlayButtonIcon = () => {
    const { isPause, isPlaying } = this.state

    if (isPlaying) {
      if (isPause) {
        return 'fa fa-play'
      } else {
        return 'fa fa-pause'
      }
    } else {
      return 'fa fa-play'
    }
  }

  getPlayControlsStyle = (mustTrue) => {
    if (mustTrue) {
      return styles.controlButtons
    } else {
      return Object.assign({}, styles.controlButtons, styles.controlButtonsDisabled)
    }
  }

  renderSpeedButtons = () => {
    let buttons = []

    _.forEach(constants.SPEED_BUTTON_VALUES, (speed, index) => {
      buttons.push(
        <button
          className="speedButton"
          key={`speedButton-${index}`}
          style={
            this.isDisableButton(speed) ? Object.assign({}, styles.speedButton, styles.disabledButton) : styles.speedButton
          }
          type="button"
          onClick={() => this.handleOnChangeSpeed(speed)}>
          {`${speed}x`}
        </button>
      )
    })

    return (
      <div style={{textAlign: 'center', marginTop: '17px'}}>
        {buttons}
      </div>
    )
  }

  render () {
    const {
      milestones,
      filteredBookingData,
      hightLightIndex,
      isPlaying,
      polylines,
      isPause
    } = this.state

    return (
      <Fragment>
        <GoogleMapWrapper
          {...this.props}
          polylines={polylines}
          isPlaying={isPlaying}
          milestones={milestones}
          onMarkerClick={this.onMarkerClick}
          filteredBookingData={filteredBookingData}
          hightLightIndex={isPlaying ? hightLightIndex : 0}/>

        {filteredBookingData.length > 0 &&
          <span style={styles.controlsWrapper}>
              <table style={{width:'100%'}}>
                <tbody style={{textAlign:'center'}}>
                  <tr>
                    <td style={{width:'20%'}}>
                      <button style={this.getPlayControlsStyle(isPlaying)}
                        type="button"
                        onClick={this.handleStopAnimation}>
                        <i className={"fa fa-stop"}/>
                      </button>
                    </td>
                    <td style={{width:'20%'}}>
                      <button style={this.getPlayControlsStyle(isPause)}
                        type="button"
                        onClick={() => this.handleOneStepMove(0)}>
                        <i className={"fa fa-step-backward"}/>
                      </button>
                    </td>
                    <td style={{width:'20%'}}>
                      <button
                        style={styles.playAndPauseButton}
                        type="button"
                        onClick={this.onPlayPauseClick}>
                        <i style={this.getPlayButtonIcon() === "fa fa-play" ? {marginLeft: '5px'} : {}}
                          className={this.getPlayButtonIcon()}/>
                      </button>
                    </td>
                    <td style={{width:'20%'}}>
                      <button style={this.getPlayControlsStyle(isPause)}
                        type="button"
                        onClick={() => this.handleOneStepMove(1)}>
                        <i className={"fa fa-step-forward"}/>
                      </button>
                    </td>
                    <td style={{width:'20%'}}>
                      <button style={this.getPlayControlsStyle(isPlaying || isPause)}
                        type="button"
                        onClick={this.handleStartOver.bind(this)}>
                        <i className={"fa fa-undo"}/>
                      </button>
                    </td>
                  </tr>
                </tbody>
              </table>

              {this.renderSpeedButtons()}
          </span>
        }
      </Fragment>
    )
  }
}

const styles = {
  disabledButton: {
    cursor: 'cursor',
    color: '#4192B7',
    border: '1px solid #4192B7'
  },
  controlsWrapper: {
    width: '350px',
    height: '140px',
    padding: '15px 0px 0px',
    position: 'absolute',
    background: '#FEFEFE',
    top: '62px',
    left: '66px',
    borderRadius: '6px',
    boxShadow: '2px 2px 2px 2px #E1E1E1'
  },
  playAndPauseButton: {
    outline: 'none',
    width: '60px',
    height: '60px',
    padding: '0px',
    border:  '1px solid #52C4B6',
    borderRadius:  '50%',
    color: '#f5f5f5',
    textAlign: 'center',
    textDecoration: 'none',
    background:  '#52C4B6',
    fontSize: '25px',
    fontWeight: 'bold'
  },
  controlButtons: {
    outline: 'none',
    fontSize: '25px',
    background: '#FEFEFE',
    color: '#57C4B6',
    border: 'none',
    width: '100%',
    height: '100%',
    // color: '#CED7DC',
  },
  controlButtonsDisabled: {
    color: '#CED7DC'
  },
  speedButton: {
    outline: 'none',
    color: '#C4D0D5',
    border: '1px solid #C4D0D5',
    borderRadius: '3px',
    width: '33px',
    height: '30px',
    padding: '0px',
    margin: '0px 10px'
  }
}

Map.propTypes = {
  defaultCenter: PropTypes.object,
  googleMapURL: PropTypes.string,
  loadingElement: PropTypes.element,
  containerElement: PropTypes.element,
  mapElement: PropTypes.element,
  style: PropTypes.object,
  zoom: PropTypes.number
}

Map.defaultProps = {
  defaultCenter: { lat: -6.2069132, lng: 106.8112517 },
  googleMapURL: `https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places&key=${constants.GOOGLE_MAP_KEY}`,
  loadingElement: <div style={{ height: '100%' }} />,
  containerElement: <div style={{ width: '100%', height: '100%' }} />,
  mapElement: <div style={{ height: '100%' }} />,
  style: null,
  zoom: 12
}

export default Map
