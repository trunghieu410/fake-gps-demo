import React, { Component } from 'react'
import { Marker, InfoWindow } from 'react-google-maps'
import {
  getLocationSpeed,
  getTimeFromMiliSecond
} from '../global/helper'
import moment from 'moment'
import 'moment-timezone'

class CustomMarker extends Component {
  constructor(props) {
    super(props)
    this.state = {
      showInfoWindow: false
    }
  }

  toogleShowInFoWinDow = () => {
    this.setState({ showInfoWindow: !this.state.showInfoWindow })
  }

  getSpeedOfLocation = () => {
    const { filteredBookingData, location, index } = this.props

    if(index === 0) return 0

    return `${getLocationSpeed(filteredBookingData[index-1], location)} km/h`
  }

  getTimeOfDriving = () => {
    let prev = ''
    const { filteredBookingData, location, index } = this.props

    if (filteredBookingData[index-1]) {
      prev = ` ${getTimeFromMiliSecond(location.seconds - filteredBookingData[index-1].seconds)}`
    }

    return prev
  }

  getDateTime = () => {
    const { location } = this.props

    let dateTime = moment((new Date(location.seconds*1000)))

    return dateTime.tz('Asia/Jakarta').format('ddd Do MMM, YYYY - h:mm A')
  }

  showInfoWindow = () => {
    const { hightLightIndex, index, isPlaying } = this.props

    if (this.state.showInfoWindow) return true

    if (hightLightIndex === index && isPlaying) return true

    return false
  }

  render() {
    const { info, position, icon, location, index, onMarkerClick, filteredBookingData } = this.props
    const title = isNaN(parseInt(info)) ? info : `GPS Ping #${parseInt(info)+1} of ${filteredBookingData.length}`

    return (
      <Marker
        onClick={() => { onMarkerClick(index) }}
        position={position}
        icon={icon}
        onMouseOver={this.toogleShowInFoWinDow}
        onMouseOut={this.toogleShowInFoWinDow}>
        {this.showInfoWindow() &&
          <InfoWindow>
            <table>
              <tbody style={styles.tootipTbody}>
                <tr>
                  <th style={styles.tooltipTd1}>{title}</th>
                  <td></td>
                </tr>
                <tr>
                  <td style={styles.tooltipTd1}>Speed:</td>
                  <td style={styles.tooltipTd2}>{this.getSpeedOfLocation()}</td>
                </tr>
                <tr>
                  <td style={styles.tooltipTd1}>Accuracy:</td>
                  <td style={styles.tooltipTd2}>{`${location.accuracyInMeters}m`}</td>
                </tr>
                <tr>
                  <td style={styles.tooltipTd1}>Altitude:</td>
                  <td style={styles.tooltipTd2}>{location.altitudeInMeters === 'null' ? '--' : `${location.accuracyInMeters}m`}</td>
                </tr>
                <tr>
                  <td style={Object.assign({}, styles.tooltipTd1, {paddingRight: '10px'})}>Time Taken from Last Ping:</td>
                  <td style={styles.tooltipTd2}>{this.getTimeOfDriving()}</td>
                </tr>
                <tr>
                  <td style={styles.tooltipTd1}>Location:</td>
                  <td style={styles.tooltipTd2}>{` ${location.coordinates}`}</td>
                </tr>
                <tr>
                  <td style={styles.tooltipTd1}>Date Time (GMT +7):</td>
                  <td style={styles.tooltipTd2}>{this.getDateTime()}</td>
                </tr>
              </tbody>
            </table>
          </InfoWindow>}
      </Marker>
    )
  }
}

const styles = {
  tooltipTd1: {
    color: '#8F9FA7'
  },
  tooltipTd2: {
    color: '#515C63'
  },
  tootipTbody: {
    textAlign:'left', fontFamily: 'Lato-Regular'
  }
}

export default CustomMarker
